package org.yakbili.exo2;

public class Main {

	public static void main(String[] args) {
		Bissextile biss = new Bissextile();
		System.out.println("1901 bissextile ? " + biss.bissextile(1901));
		System.out.println("1996 bissextile ? " + biss.bissextile(1996));
		System.out.println("1900 bissextile ? " + biss.bissextile(1900));
		System.out.println("2000 bissextile ? " + biss.bissextile(2000));
	}

}
