package org.yakbili.exo1;

public class Factorielle {
	public int intFactorielle(int n) {
		
		if(n>1) return n*intFactorielle(n-1);
		else return 1;
		
	}
	
	public double doubleFactorielle(int n) {
		
		if(n>1) return n*doubleFactorielle(n-1);
		else return 1;
		
	}
	
	
}
