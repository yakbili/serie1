package org.yakbili.exo4;

public class Main {

	public static void main(String[] args) {
		Palindrome pal = new Palindrome();
		System.out.println("les phrasese suivantes sont-elles des palindromes?");
		System.out.println("rions noir : " + pal.palindrome("rions noir"));
		System.out.println("esope reste ici et se repose : " + pal.palindrome("esope reste ici et se repose"));
		System.out.println("elu par cette crapule : " + pal.palindrome("elu par cette crapule"));
		System.out.println("et la marine va venir a malte : " + pal.palindrome("et la marine va venir a malte"));
		//NB : sensible � la casse !

	}

}
