package org.yakbili.exo4;

public class Palindrome {
	public boolean palindrome(String s) {
		String st = s.replaceAll(" ",""); //on retire les espaces
		int n;
		n = st.length(); //n prend la taille de s sans espaces
		
		Character c1, c2;
		
		for(int i=0; i<=n/2; i++) {
			c1 = st.charAt(0+i); //c1 = premier charactère
			c2 = st.charAt(n-1-i); //c2= derniers charactère
			int c = c1.compareTo(c2); //c=0 si c1=c2
			if(c !=0) {
				return false;
			}
		}
		return true;

	}

}
