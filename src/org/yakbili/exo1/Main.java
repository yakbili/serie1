package org.yakbili.exo1;

public class Main {

	public static void main(String[] args) {
		Factorielle fact = new Factorielle();
		System.out.println("intFactorielle de 10 = " + fact.intFactorielle(10));
		System.out.println("doubleFactorielle de 10 = " + fact.doubleFactorielle(10));

		System.out.println("intFactorielle de 13 = " + fact.intFactorielle(13));
		System.out.println("doubleFactorielle de 13 = " + fact.doubleFactorielle(13));

		System.out.println("intFactorielle de 25 = " + fact.intFactorielle(25));
		System.out.println("doubleFactorielle de 25 = " + fact.doubleFactorielle(25));

	}

}
